package fr.isen.java2.db.daos;

import static fr.isen.java2.db.daos.DataSourceFactory.getDataSource;

import java.util.List;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import fr.isen.java2.db.entities.Film;
import fr.isen.java2.db.entities.Genre;


public class FilmDao {

	public List<Film> listFilms() {
		List<Film> listOfFilm = new ArrayList<>();
	    try (Connection connection = getDataSource().getConnection()) {
	        try (Statement statement = connection.createStatement()) {
	            try (ResultSet results = statement.executeQuery("select * from film JOIN genre ON film.genre_id = genre.idgenre")) {
	                while (results.next()) {
	                	Film film = new Film(results.getInt("idfilm"),
	                			results.getString("title"),
	                			results.getDate("release_date").toLocalDate(),
	                			new Genre(results.getInt("idgenre"),
	                					results.getString("name")),
	                			results.getInt("duration"),
	                			results.getString("director"),
	                			results.getString("summary"));
	                	listOfFilm.add(film);
	                }
	            }
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    return listOfFilm;
	}

	public List<Film> listFilmsByGenre(String genreName) {
		List<Film> listOfFilmByGenre = new ArrayList<>();
	    try (Connection connection = getDataSource().getConnection()) {
	        try (PreparedStatement statement = connection.prepareStatement(
	        		"SELECT * FROM film JOIN genre ON film.genre_id = genre.idgenre WHERE genre.name =?")) {
	        	statement.setString(1, genreName);
	        	try (ResultSet results = statement.executeQuery()) {
	                while (results.next()) {
	                	Film film = new Film(results.getInt("idfilm"),
	                			results.getString("title"),
	                			results.getDate("release_date").toLocalDate(),
	                			new Genre(results.getInt("idgenre"),
	                					results.getString("name")),
	                			results.getInt("duration"),
	                			results.getString("director"),
	                			results.getString("summary"));
	                	listOfFilmByGenre.add(film);
	                }
	            }
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    return listOfFilmByGenre;
	}

	public Film addFilm(Film film) {
		try (Connection connection = getDataSource().getConnection()) {
	        String sqlQuery = "INSERT INTO film(title,release_date,genre_id,duration,director,summary) VALUES(?,?,?,?,?,?)";
	        try (PreparedStatement statement = connection.prepareStatement(
	                        sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
	            statement.setString(1, film.getTitle());
	            statement.setDate(2, Date.valueOf(film.getReleaseDate()));
	            statement.setInt(3, film.getGenre().getId());
	            statement.setInt(4, film.getDuration());
	            statement.setString(5, film.getDirector());
	            statement.setString(6, film.getSummary());
	            statement.executeUpdate();
	            film.setId(statement.getGeneratedKeys().getInt(1));

	        }
	    }catch (SQLException e) {
	        e.printStackTrace();
	    }
		return film;
	}
}

